import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Food Searching App</h1>
      <form  className="search-form">
      <input
          type="text"
          placeholder="Search Food"
        />
        <input type="submit" value="Search" />
      </form>

    </div>
  );
}

export default App;
